// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "src/jmkdtree.h"




std::vector< Vec3 > positions;
std::vector< Vec3 > normals;

std::vector< Vec3 > positions2;
std::vector< Vec3 > normals2;

std::vector<Vec3> positions3 = std::vector<Vec3>();
std::vector<Vec3> normals3 = std::vector<Vec3>();

std::vector<Vec3> positionsTemp = std::vector<Vec3>();
std::vector<Vec3> normalsTemp = std::vector<Vec3>();


// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 640;
static unsigned int SCREENHEIGHT = 480;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static bool fullScreen = false;




// ------------------------------------------------------------------------------------------------------------
// i/o and some stuff
// ------------------------------------------------------------------------------------------------------------
void loadPN (const std::string & filename , std::vector< Vec3 > & o_positions , std::vector< Vec3 > & o_normals ) {
    unsigned int surfelSize = 6;
    FILE * in = fopen (filename.c_str (), "rb");
    if (in == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    size_t READ_BUFFER_SIZE = 1000; // for example...
    float * pn = new float[surfelSize*READ_BUFFER_SIZE];
    o_positions.clear ();
    o_normals.clear ();
    while (!feof (in)) {
        unsigned numOfPoints = fread (pn, 4, surfelSize*READ_BUFFER_SIZE, in);
        for (unsigned int i = 0; i < numOfPoints; i += surfelSize) {
            o_positions.push_back (Vec3 (pn[i], pn[i+1], pn[i+2]));
            o_normals.push_back (Vec3 (pn[i+3], pn[i+4], pn[i+5]));
        }

        if (numOfPoints < surfelSize*READ_BUFFER_SIZE) break;
    }
    fclose (in);
    delete [] pn;
}
void savePN (const std::string & filename , std::vector< Vec3 > const & o_positions , std::vector< Vec3 > const & o_normals ) {
    if ( o_positions.size() != o_normals.size() ) {
        std::cout << "The pointset you are trying to save does not contain the same number of points and normals." << std::endl;
        return;
    }
    FILE * outfile = fopen (filename.c_str (), "wb");
    if (outfile == NULL) {
        std::cout << filename << " is not a valid PN file." << std::endl;
        return;
    }
    for(unsigned int pIt = 0 ; pIt < o_positions.size() ; ++pIt) {
        fwrite (&(o_positions[pIt]) , sizeof(float), 3, outfile);
        fwrite (&(o_normals[pIt]) , sizeof(float), 3, outfile);
    }
    fclose (outfile);
}
void scaleAndCenter( std::vector< Vec3 > & io_positions ) {
    Vec3 bboxMin( FLT_MAX , FLT_MAX , FLT_MAX );
    Vec3 bboxMax( FLT_MIN , FLT_MIN , FLT_MIN );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        for( unsigned int coord = 0 ; coord < 3 ; ++coord ) {
            bboxMin[coord] = std::min<float>( bboxMin[coord] , io_positions[pIt][coord] );
            bboxMax[coord] = std::max<float>( bboxMax[coord] , io_positions[pIt][coord] );
        }
    }
    Vec3 bboxCenter = (bboxMin + bboxMax) / 2.f;
    float bboxLongestAxis = std::max<float>( bboxMax[0]-bboxMin[0] , std::max<float>( bboxMax[1]-bboxMin[1] , bboxMax[2]-bboxMin[2] ) );
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = (io_positions[pIt] - bboxCenter) / bboxLongestAxis;
    }
}

void applyRandomRigidTransformation( std::vector< Vec3 > & io_positions , std::vector< Vec3 > & io_normals ) {
    srand(time(NULL));
    Mat3 R = Mat3::RandRotation();
    Vec3 t = Vec3::Rand(1.f);
    for(unsigned int pIt = 0 ; pIt < io_positions.size() ; ++pIt) {
        io_positions[pIt] = R * io_positions[pIt] + t;
        io_normals[pIt] = R * io_normals[pIt];
    }
}

void subsample( std::vector< Vec3 > & i_positions , std::vector< Vec3 > & i_normals , float minimumAmount = 0.1f , float maximumAmount = 0.2f ) {
    std::vector< Vec3 > newPos , newNormals;
    std::vector< unsigned int > indices(i_positions.size());
    for( unsigned int i = 0 ; i < indices.size() ; ++i ) indices[i] = i;
    srand(time(NULL));
    std::random_shuffle(indices.begin() , indices.end());
    unsigned int newSize = indices.size() * (minimumAmount + (maximumAmount-minimumAmount)*(float)(rand()) / (float)(RAND_MAX));
    newPos.resize( newSize );
    newNormals.resize( newSize );
    for( unsigned int i = 0 ; i < newPos.size() ; ++i ) {
        newPos[i] = i_positions[ indices[i] ];
        newNormals[i] = i_normals[ indices[i] ];
    }
    i_positions = newPos;
    i_normals = newNormals;
}

bool save( const std::string & filename , std::vector< Vec3 > & vertices , std::vector< unsigned int > & triangles ) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl;

    unsigned int n_vertices = vertices.size() , n_triangles = triangles.size()/3;
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for( unsigned int v = 0 ; v < n_vertices ; ++v ) {
        myfile << vertices[v][0] << " " << vertices[v][1] << " " << vertices[v][2] << std::endl;
    }
    for( unsigned int f = 0 ; f < n_triangles ; ++f ) {
        myfile << 3 << " " << triangles[3*f] << " " << triangles[3*f+1] << " " << triangles[3*f+2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}



// ------------------------------------------------------------------------------------------------------------
// rendering.
// ------------------------------------------------------------------------------------------------------------

void initLight () {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f,-16.0f,-50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv (GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv (GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv (GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable (GL_LIGHT1);
    glEnable (GL_LIGHTING);
}

void init () {
    camera.resize (SCREENWIDTH, SCREENHEIGHT);
    initLight ();
    glCullFace (GL_BACK);
    glEnable (GL_CULL_FACE);
    glDepthFunc (GL_LESS);
    glEnable (GL_DEPTH_TEST);
    glClearColor (0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
}



void drawTriangleMesh( std::vector< Vec3 > const & i_positions , std::vector< unsigned int > const & i_triangles ) {
    glBegin(GL_TRIANGLES);
    for(unsigned int tIt = 0 ; tIt < i_triangles.size() / 3 ; ++tIt) {
        Vec3 p0 = i_positions[3*tIt];
        Vec3 p1 = i_positions[3*tIt+1];
        Vec3 p2 = i_positions[3*tIt+2];
        Vec3 n = Vec3::cross(p1-p0 , p2-p0);
        n.normalize();
        glNormal3f( n[0] , n[1] , n[2] );
        glVertex3f( p0[0] , p0[1] , p0[2] );
        glVertex3f( p1[0] , p1[1] , p1[2] );
        glVertex3f( p2[0] , p2[1] , p2[2] );
    }
    glEnd();
}

void drawPointSet( std::vector< Vec3 > const & i_positions , std::vector< Vec3 > const & i_normals ) {
    glBegin(GL_POINTS);
    for(unsigned int pIt = 0 ; pIt < i_positions.size() ; ++pIt) {
        glNormal3f( i_normals[pIt][0] , i_normals[pIt][1] , i_normals[pIt][2] );
        glVertex3f( i_positions[pIt][0] , i_positions[pIt][1] , i_positions[pIt][2] );
    }
    glEnd();
}

void draw () {
    glPointSize(2); // for example...

    glColor3f(0.8,0.8,1);
    drawPointSet(positions , normals);

    // glColor3f(1,0.5,0.5);
    // drawPointSet(positions2 , normals2);
    //0.7,1,0.7
    glColor3f(1,0.5,0.5);
    drawPointSet(positions3 , normals3);
}

void drawMesh(){
    glPointSize(2);
    glColor3f(0.8,0.8,1);
    drawPointSet(positions , normals);
}








void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    draw ();
    glFlush ();
    glutSwapBuffers ();
}


void displayMesh () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply ();
    drawMesh ();
    glFlush ();
    glutSwapBuffers ();
}

void idle () {
    glutPostRedisplay ();
}

void key (unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
    case 'f':
        if (fullScreen == true) {
            glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
            fullScreen = false;
        } else {
            glutFullScreen ();
            fullScreen = true;
        }
        break;
    
    case 'o':
        display();
        break;
    case 'i':
        displayMesh();
        break;

    case 'w':
        GLint polygonMode[2];
        glGetIntegerv(GL_POLYGON_MODE, polygonMode);
        if(polygonMode[0] != GL_FILL)
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
        else
            glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
        break;

    default:
        break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate (x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
        camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    }
    else if (mouseZoomPressed == true) {
        camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
        lastZoom = y;
    }
}


void reshape(int w, int h) {
    camera.resize (w, h);
}


Vec3 projOnPlane(Vec3 point, Vec3 normal, Vec3 pop){


    Vec3 cx = point - pop; 
    return point - Vec3::dot(cx, normal)*normal;

/*
    float norme = Vec3::dot(normal,Vec3(pop[0]-point[0],pop[1]-point[1],pop[2]-point[2]));
    //norme/=Vec3::dot(normal,normal);
    //std::cout<<pop[0]<<" "<<pop[1]<<" "<<pop[2]<<std::endl;
    norme/=(normal.length());

    Vec3 res = Vec3(point[0] - (normal[0]*norme),point[1] - (normal[1]*norme),point[2] - (normal[2]*norme));
    //std::cout<<res[0]<<" "<<res[1]<<" "<<res[2]<<std::endl;
    return res;
    */
}


void APSS(Vec3 inputPoint, Vec3& outputPoint, Vec3& outputNormal, 
    std::vector<Vec3> const & positions, std::vector<Vec3> const & normals, 
    BasicANNkdTree const & kdtree, int kernelType, float h, unsigned int nbIter = 10, 
    unsigned int knn = 20){

    
    Vec3 pointDep = inputPoint;

    for (int iter = 0 ; iter < nbIter; iter++){

    

        ANNidxArray id_nearest = new ANNidx[ knn ];
        ANNdistArray dists = new ANNdist[ knn ];
        kdtree.knearest(pointDep, knn, id_nearest, dists);

        float max = dists[0];
        for (int i =1;i<knn;i++){
            if (dists[i]>max){max=dists[i];}
        }
        

        std::vector<Vec3> listeProjs = std::vector<Vec3>();
        std::vector<Vec3> neighbours = std::vector<Vec3>();
        std::vector<float> weightList = std::vector<float>();
        std::vector<float> normedWeightList = std::vector<float>();

        for (int i = 0; i< knn; i++){
            neighbours.push_back(positions[id_nearest[i]]);
            Vec3 norm = normals[id_nearest[i]];
            // switch (kernelType) {
            //     case 1 : ; break;
            //     case 2 : ; break;
            //     default : ;break;
            // }
            weightList.push_back(exp(-(dists[i]*dists[i])/(max*max)));
            normedWeightList.push_back(weightList[i]);
        }
        Vec3 resP = Vec3(0.0,0.0,0.0);
        Vec3 resN = Vec3(0.0,0.0,0.0);
        float weightSum=0.0;
        for (int i =0;i<knn;i++){
            weightSum+=weightList[i];
        }
        for (int i =0; i<knn;i++){
            normedWeightList[i]/=weightSum;
        }

        double u4 = 0.0;
        for(int i=0;i<knn;i++){
            u4+=weightList[i]*Vec3::dot(neighbours[i],normals[i]);
        }

        for (int i=0;i<knn;i++){
            Vec3 sum = Vec3(0.0,0.0,0.0);
            for (int j=0;j<knn;j++){
                sum+=weightList[j]*normals[j];
            }
            u4-=(normedWeightList[i]*Vec3::dot(neighbours[i],sum));
        }

        u4/=2.0;

        double denom=0.0;

        for (int i=0;i<knn;i++){
            denom+=weightList[i]*Vec3::dot(neighbours[i],neighbours[i]);
        }
        for(int i=0;i<knn;i++){
            Vec3 sum=Vec3(0.0,0.0,0.0);
            for (int j=0;j<knn;j++){
                sum+=weightList[j]*neighbours[i];
            }
            denom-=normedWeightList[i]*Vec3::dot(neighbours[i],sum);
        }
        u4=u4/denom;

        Vec3 ui = Vec3(0.0,0.0,0.0);
        for (int i=0;i<knn;i++){
            ui+=normedWeightList[i]*normals[i];
        }
        for (int i=0;i<knn;i++){
            ui-=2*u4*normedWeightList[i]*neighbours[i];
        }


        Vec3 temp = Vec3(0.0,0.0,0.0);
        for(int i=0;i<knn;i++){
            temp+=normedWeightList[i]*neighbours[i];
        }
        double u0=(-1)*(Vec3::dot(ui,temp));

        double sum=0.0;

        for (int i=0;i<knn;i++){
            sum+=normedWeightList[i]*Vec3::dot(neighbours[i],neighbours[i]);
        }

        u0-=u4*sum;

        
        Vec3 center = -1.0*ui;
        center/=(2.0*u4);

        float radius = sqrt(Vec3::dot(center,center)-(u0/u4));

        outputNormal = (ui + 2*u4*pointDep);
        outputNormal /= Vec3::dot(outputNormal,outputNormal);

        // outputNormal=(pointDep-center);
        // outputNormal/=Vec3::dot(outputNormal,outputNormal);
        outputPoint=center+radius*outputNormal;


        delete[] id_nearest;
        delete[] dists;
        pointDep = outputPoint;
    }

    
}


void HPSS(Vec3 inputPoint, Vec3& outputPoint, Vec3& outputNormal, 
    std::vector<Vec3> const & positions, std::vector<Vec3> const & normals, 
    BasicANNkdTree const & kdtree, int kernelType, float h, unsigned int nbIter = 10, 
    unsigned int knn = 20){


//     int idNearest = kdtree.nearest(inputPoint); 

//     outputPoint = projOnPlane(inputPoint, normals[idNearest], positions[idNearest]); 
//     outputNormal = normals[idNearest];
// return ; 
    
    //BasicANNkdTree kd_tree;
    //kd_tree.build(positions);

    Vec3 pointDep = inputPoint;

    for (int iter = 0 ; iter < nbIter; iter++){

    

        ANNidxArray id_nearest = new ANNidx[ knn ];
        ANNdistArray dists = new ANNdist[ knn ];
        kdtree.knearest(pointDep, knn, id_nearest, dists);

        float max = dists[0];
        for (int i =1;i<knn;i++){
            if (dists[i]>max){max=dists[i];}
        }
        

        std::vector<Vec3> listeProjs = std::vector<Vec3>();
        std::vector<float> weightList = std::vector<float>();

        for (int i = 0; i< knn; i++){
            Vec3 point = positions[id_nearest[i]];
            Vec3 norm = normals[id_nearest[i]];

            listeProjs.push_back(projOnPlane(pointDep, norm, point));
            // switch (kernelType) {
            //     case 1 : ; break;
            //     case 2 : ; break;
            //     default : ;break;
            // }
            weightList.push_back(exp(-(dists[i]*dists[i])/max*max*100));

            //std::cout<<proj[0]<<" "<<proj[1]<<" "<<proj[2]<<std::endl;
        }
        Vec3 resP = Vec3(0.0,0.0,0.0);
        Vec3 resN = Vec3(0.0,0.0,0.0);
        float weightSum=0.0;
        for (int i =0;i<knn;i++){
            resP[0]+=weightList[i]*listeProjs[i][0];
            resP[1]+=weightList[i]*listeProjs[i][1];
            resP[2]+=weightList[i]*listeProjs[i][2];
            resN[0]+=weightList[i]*normals[id_nearest[i]][0];
            resN[1]+=weightList[i]*normals[id_nearest[i]][1];
            resN[2]+=weightList[i]*normals[id_nearest[i]][2];
            weightSum+=weightList[i];

            // resP[0]+=listeProjs[i][0];
            // resP[1]+=listeProjs[i][1];
            // resP[2]+=listeProjs[i][2];
            // resN[0]+=normals[i][0];
            // resN[1]+=normals[i][1];
            // resN[2]+=normals[i][2];
        }

        
        resP[0]/=weightSum;
        resP[1]/=weightSum;
        resP[2]/=weightSum;
        resN[0]/=weightSum;
        resN[1]/=weightSum;
        resN[2]/=weightSum;

        // resP[0]/=knn;
        // resP[1]/=knn;
        // resP[2]/=knn;
        // resN[0]/=knn;
        // resN[1]/=knn;
        // resN[2]/=knn;

        outputPoint=resP;
        outputNormal=resN;


        delete[] id_nearest;
        delete[] dists;
        pointDep = resP;
    }

}



int main (int argc, char ** argv) {
    if (argc > 2) {
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("tp point processing");

    init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);


    {
        // Load a first pointset, and build a kd-tree:
        loadPN("pointsets/igea.pn" , positions , normals);

        // for (int i=0;i<normals.size();i++){
        //     normals[i][0]+=((rand()%5)/10) - 0.25;
        //     normals[i][1]+=((rand()%10)/10) - 0.25;
        //     normals[i][2]+=((rand()%10)/10) - 0.25;
            
        // }

        float noise = 0.01;
        //float maxSpread=2.0;

        for (int i=0;i<positions.size();i++){
            positions[i][0]+=(double)rand()/(double)(RAND_MAX)*2.0*noise-noise;
            positions[i][1]+=(double)rand()/(double)(RAND_MAX)*2.0*noise-noise;
            positions[i][2]+=(double)rand()/(double)(RAND_MAX)*2.0*noise-noise;
            // positions[i][1]+=(float)((rand()%100 - 50)/100.0*((int)(noisePercent*maxSpread)));
            // positions[i][2]+=(float)((rand()%100 - 50)/100.0*((int)(noisePercent*maxSpread)));           
        }
        
        

        BasicANNkdTree kdtree;
        kdtree.build(positions);

        // Create a second pointset that is artificial, and project it on pointset1 using MLS techniques:
        positions2.resize( 100000 );
        normals2.resize(positions2.size());
        for( unsigned int pIt = 0 ; pIt < positions2.size() ; ++pIt ) {
            positions2[pIt] = Vec3(
                        -0.6 + 1.2 * (double)(rand())/(double)(RAND_MAX),
                        -0.6 + 1.2 * (double)(rand())/(double)(RAND_MAX),
                        -0.6 + 1.2 * (double)(rand())/(double)(RAND_MAX)
                        );
            positions2[pIt].normalize();
            positions2[pIt] = 0.6 * positions2[pIt];
        }

        // PROJECT USING MLS (HPSS and APSS):
        // TODO
        Vec3 test1 = Vec3();
        Vec3 test2 = Vec3();

        for (int pnt =0;pnt<positions2.size();pnt++){
            
            HPSS(positions2[pnt], test1, test2, positions, normals, kdtree, 0, 1.0 );
            positions3.push_back(test1);
            normals3.push_back(test2);
        }
        //positions2=positions3;
    }



    glutMainLoop ();
    return EXIT_SUCCESS;
}

